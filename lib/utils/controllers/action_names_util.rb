module Utils
  module Controllers
    module ActionNamesUtil

      def self.included(base)
        base.class_eval do
          helper_method :action_index?, :action_show?, :action_edit?, :action_update?, :action_edit_or_update?, :action_new?, :action_create?, :action_new_or_edit?, :action_new_or_create?, :action_create_or_update?
        end
      end

      def action_index?
        'index' == action_name
      end

      def action_show?
        'show' == action_name
      end

      def action_edit?
        action_name == 'edit'
      end

      def action_update?
        action_name == 'update'
      end

      def action_edit_or_update?
        action_edit? || action_update?
      end

      def action_new?
        action_name == 'new'
      end

      def action_create?
        action_name == 'create'
      end

      def action_new_or_edit?
        action_new? || action_edit?
      end

      def action_new_or_create?
        action_new? || action_create?
      end

      def action_create_or_update?
        action_create? || action_update?
      end

    end
  end
end
