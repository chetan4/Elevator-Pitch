namespace :redis do
 desc "Flushes all data from Redis and adds them back"
 task :flushall => :environment do
  puts 'Flushing Redis server'
  
$redis.client.disconnect  if $redis && $redis.client
      $redis = Redis.new(:host => 'localhost', :port => 6379)
  $redis.flushall

  puts 'Initializing Redis server....'
  Pitch.all.each do |pitch|
  tags = pitch.tags
    pitch.tag_list.each do |tag|
     $redis.sadd(tag.downcase.squish,pitch.id)
    end
  end
  puts 'Redis server initialized!'
 end
end