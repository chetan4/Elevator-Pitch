$(document).ready(function(){
	$('.first_phrase').keyup(function(element){
		highlight(element, $('.first_phrase_match_status'));
	});

	$('.second_phrase').keyup(function(element){
		highlight(element,$('.second_phrase_match_status'));
	});

});

function highlight(element,status_field){
	typed_text = $(element.target).val();
	matched = false;

	$('.seo-settings div.pitch p').each(function(index,value){
		paragraph = $(value).text();
		regExp = new RegExp(typed_text, 'i');
		highighted_paragraph = paragraph.replace(regExp, "<em>"+ typed_text+"</em>");
		if (paragraph.match(regExp) != null && typed_text.length > 0){
			matched = true;
		}
		$(status_field).show();
		$(value).html(highighted_paragraph);
	});

	if (matched){
		$(status_field).html("<em class='success'>Perfect. We found those terms in your pitch.</em>");
	}else{
		$(status_field).html("<em class='warn'>Oops! We cannot find those words in your pitch.</em>");
	}
	$(status_field).show();
}