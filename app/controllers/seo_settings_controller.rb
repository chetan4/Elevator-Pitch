class SeoSettingsController < ApplicationController
  before_filter :authenticate_user!, :except => [:display, :remember]

  def new
  	@seo_setting = SeoSetting.new(:pitch => Pitch.find_by_identifier(params[:identifier]))
  end

  def create
  	@seo_setting = SeoSetting.new(params[:seo_setting])
  	if @seo_setting.save
  		current_user.email.present? ? redirect_to(pitch_path) : redirect_to(edit_user_registration_path)
  	else
  		render :action => :new ,:identifier => @seo_setting.pitch.identifier
  	end
  end

  def edit
    @seo_setting = current_user.pitch.seo_setting
  end

  def update
    @seo_setting = current_user.pitch.seo_setting
    @seo_setting.attributes = params[:seo_setting]
    @seo_setting.save!
    if !@current_user.email.present?
      redirect_to edit_user_registration_path
    else
      redirect_to(pitch_path(@seo_setting.pitch))
    end
  rescue ActiveRecord::RecordInvalid
    render :action => "edit"
  end

  
end
