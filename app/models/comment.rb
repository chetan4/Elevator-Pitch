class Comment < ActiveRecord::Base

  include ActsAsCommentable::Comment

  belongs_to :commentable, :polymorphic => true

  default_scope :order => 'created_at ASC'

  # NOTE: install the acts_as_votable plugin if you
  # want user to vote on the quality of comments.
  #acts_as_voteable

  # NOTE: Comments belong to a user
  belongs_to :user
  
  after_create :notify_new_comment
  
  
  def notify_new_comment
    all_commenter_ids = Comment.where(:commentable_id => self.commentable_id).uniq.pluck(:user_id)
    commenter_emails =  User.where("id in (#{all_commenter_ids.join(',')})").uniq.pluck(:email)
    if commenting_on_self_pitch?
      CommentNotifications.delay.notify_all_commenters(self,commenter_emails)
    else
      CommentNotifications.delay.notify_pitcher(self)
      CommentNotifications.delay.notify_all_commenters(self,commenter_emails)
    end
  rescue
    nil
  end

  private
  def commenting_on_self_pitch?
    self.user_id == self.commentable.user.id
  end
end
