class SeoSetting < ActiveRecord::Base
  attr_accessible :first_phrase, :first_url, :second_phrase, :second_url, :page_title, :page_description,:pitch_id,:pitch
  belongs_to :pitch

  VALID_URL_REGEX_SET = /^(https?:\/\/(w{3}\.)?)|(w{3}\.)|[a-z0-9]+(?:[\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(?:(?::[0-9]{1,5})?\/[^\s]*)?/ix
  validates :first_url, :format => {:allow_blank => true, :with => VALID_URL_REGEX_SET}
  validates :second_url, :format => {:allow_blank => true, :with => VALID_URL_REGEX_SET}

  validates :page_title, :length => { :maximum => 70 }
  validates :page_description, :length => { :maximum => 140 }


  

end
