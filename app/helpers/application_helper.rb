module ApplicationHelper
  include TweetButton
  TweetButton.default_tweet_button_options = {:via => "MindYourPitch", :count => "none"}
  
  def get_user_status(user)
    status = user.is_individual
    if status.nil?
      return true
    else
      return status
    end
  end

  def configure_seo(text, pitch)
    return text unless pitch.seo_settings_configured?
    seo_setting = pitch.seo_setting
    text = text.sub(/#{seo_setting.first_phrase}/i, "<a href='#{seo_setting.first_url}'> #{seo_setting.first_phrase} </a>")
    text = text.sub(/#{seo_setting.second_phrase}/i,"<a href='#{seo_setting.second_url}'> #{seo_setting.second_phrase} </a>")
  end

  def show_introduction?
    current_user.nil? && controller_path == 'home' && action_name == 'index'
  end

  def all_categories
    Category.all
  end

  def is_link_active(con, act)
    return "is_nav_active" if ( con == params[:controller] && act == params[:action] )
  end

  def comment_timestamp(comment)
    comment.created_at.strftime(COMMENT_TIME_FORMAT)
  end
  
  def show_statistics?
    user_signed_in? && pitch_created? && controller_path == 'home' && action_name == 'index'
  end
  
  def show_poll?
    user_signed_in? && pitch_created? && controller_path == 'home' && action_name == 'index'
  end
  
  def pitch_created?
    current_user && !current_user.pitch.blank?
  end

  def create_pitch_callout?
    !pitch_created? && !editing_profile?
  end

  def editing_profile?
    controller_path == 'registrations' && ['edit','change_password','update'].include?(action_name)
  end
  
end
