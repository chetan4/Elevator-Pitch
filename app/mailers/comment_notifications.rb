class CommentNotifications < ActionMailer::Base
  default :from => "service@mindyourpitch.com",
    :reply_to => "service@mindyourpitch.com"

  def notify_all_commenters(comment,commeter_emails = [])
  	@comment = comment
    @pitch = comment.commentable
    @pitcher = @pitch.user
    @commenter = @comment.user
    
    commenter_emails.each do |email|
   	 mail(:to => email,:subject => "New comment on the pitch you commented on") unless [@commenter.email, @pitcher.email].include?(email)
	end
  end

  def notify_pitcher(comment) 
    @comment = comment
    @pitch = comment.commentable
    @pitcher = @pitch.user
    @commenter = @comment.user
    
    mail(:to => @pitcher.email,:subject => "New comment on your pitch") if @pitcher.email.present?
  end
end
