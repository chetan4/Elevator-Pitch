# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
ElevatorPitch::Application.initialize!

COMMENT_TIME_FORMAT = '%B %d, %Y at %I:%M %p'
