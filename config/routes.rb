ElevatorPitch::Application.routes.draw do

  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config
  devise_for :users, :controllers => { :sessions => "sessions", :registrations => "registrations" }

  get 'pitches/:identifier', :to => 'pitches#display', :as => "display_pitch"
  post 'pitches/:pitch_id/likes', :to => 'likes#create', :as => "pitch_likes"
  get 'pitches/:pitch_id/likes/remember', :to => 'likes#remember', :as => "remember_pitch_likes"
  post 'pitches/:pitch_id/comments', :to => 'comments#create', :as => "pitch_comments"
  

  resource :pitch do
    get 'remember'
  end
  match '/pitch/check_identifier_availibility' => 'pitches#check_identifier_availibility'

  
  resources :seo_settings 

  resources :pitch_requests, :only => [:new, :create]

  resources :home, :only => [:index, :show] do
    collection do 
      get 'failure'
    end
  end
  resources :polls, :only => [:create]
  
  resources :companies, :only => [:update] 
  resources :contacts, :only => [:create]
  
  devise_scope :user do 
    match "/change-password" => 'registrations#change_password' ,:as => :change_password
  end


  match '/auth/:provider/callback' => 'authentications#create'
  match '/auth/failure' => 'home#failure'
  match '/contact-us' => 'contacts#new', :as => :contact_us
  match '/need-for-elevator-pitch' => "home#why_pitch", :as => :why_pitch
  match '/elevator-pitch-framework' => "home#elevator_pitch_framework", :as => :elevator_pitch_framework
  match '/how_it_works' => "home#how_it_works", :as => :how_it_works 
  match '/registration_successful' => "home#registration_successful", :as => :registration_successful
  match '/lookup' => "search#lookup", :as => :lookup

  ################## OLD ROUTES - START #################
   match '/why-pitch' => redirect("/elevator-pitch-framework")

  ################## OLD ROUTES - END #################

  
  root :to => 'home#index'
  get '/:clicked_tag', :to => 'home#index', :as => 'tag_search'



end
