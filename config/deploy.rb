if ENV['rvm_path'] != nil && ENV['rvm_path'] != ''
  $:.unshift(File.expand_path('./lib', ENV['rvm_path']))
  set :rvm_type, :user
  set :rvm_ruby_string, "ruby-1.9.3-p194@elevator-pitch"
  set :use_sudo, false
end


set :stages, %w(staging production)
set :default_stage, "staging"

require 'capistrano/ext/multistage'
require "delayed/recipes"

set :scm, :git
set :application, "elevator-pitch"
set :repository,  "git@github.com:idyllicsoftware/Elevator-Pitch.git"
set :user, "ubuntu"
set :use_sudo, false
set :deploy_to, "/usr/local/apps/#{application}"
set :deploy_via, :copy
set :repository_cache, "elevator-pitch-cache"
set :keep_releases, 3

require 'bundler/capistrano'

default_run_options[:pty] = true
ssh_options[:forward_agent] = true
#Configure SSH options for ec2
ssh_options[:keys] = [File.join(ENV["HOME"], ".ssh", "id_rsa")] unless 'development' == rails_env

before "deploy:restart", "deploy:blog"
before "deploy:restart", "deploy:migrate"
after "deploy:update_code", "deploy:web:disable"
after "deploy:update_code", "deploy:copy_database_yml"
after "deploy:update_code", "deploy:assets:precompile"
after "deploy:restart", "deploy:cleanup"
after "deploy:restart", "deploy:web:enable"
# Delayed Job
after "deploy:stop",    "delayed_job:stop"
after "deploy:start",   "delayed_job:start"
after "deploy:restart", "delayed_job:restart"
after "deploy:restart", "redis:flush"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end

  desc "task to create a symlink for the database files."
  task :copy_database_yml, :roles => :app do
    run "cd #{release_path} && cp config/database.template.yml config/database.yml"
  end

  namespace :assets do
    task :precompile, :roles => :web, :except => { :no_release => true } do
      run %Q{cd #{release_path} && RAILS_ENV=#{rails_env} bundle exec rake assets:clean && RAILS_ENV=#{rails_env} && bundle exec rake assets:precompile}
    end
  end
  
  desc "Run database migrations"
  task :migrate, :roles => :db do
    run "cd #{release_path} &&
    RAILS_ENV=#{rails_env || 'development'} bundle exec rake db:migrate"
  end
  
  desc "Create a soft link to the blog"
  task :blog, :roles => :app do
    run "cd #{release_path} && cd public && 
    ln -nfs /usr/local/apps/wordpress blog"
  end  

  #show hide maintenance page
  namespace :web do
    task :disable, :roles => :web, :except => { :no_release => true } do
      require 'erb'
      on_rollback { run "rm #{shared_path}/system/maintenance.html" }

      reason = ENV['REASON']
      deadline = ENV['UNTIL']

      template = File.read(File.join(File.dirname(__FILE__), "deploy",
          "maintenance.html.erb"))
      result = ERB.new(template).result(binding)

      put result, "#{shared_path}/system/maintenance.html", :mode => 0644
    end

    task :enable, :roles => :web, :except => { :no_release => true } do
      run "rm #{shared_path}/system/maintenance.html"
    end
  end
end

namespace :redis do
  task :flush, :roles => :app do
    run %Q{cd #{release_path} && RAILS_ENV=#{rails_env} bundle exec rake redis:flushall}
  end
end
