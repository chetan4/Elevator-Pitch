Rails.application.config.middleware.use OmniAuth::Builder do
  provider :twitter, 'XTrkQzdOQjJ3OzI0Wb1Q', 'wzIPb00bWIkmjxUkFXCe0mdrT6Q62yPeA3W9OKtPa1c'
  provider :facebook, FACEBOOK_CONFIG[:app_id], FACEBOOK_CONFIG[:app_secret]

  #401 unauthorised error
  OmniAuth.config.on_failure = Proc.new { |env|
      OmniAuth::FailureEndpoint.new(env).redirect_to_failure
  }
end
