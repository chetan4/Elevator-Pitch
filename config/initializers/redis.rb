if Rails.env == 'development'
  $redis = Redis.new(:host => 'localhost', :port => 6379)
end

if defined?(PhusionPassenger)
  PhusionPassenger.on_event(:starting_worker_process) do |forked|
    # We're in smart spawning mode.
    if forked
      # Re-establish redis connection
      require 'redis'
     
      $redis.client.disconnect  if $redis && $redis.client
      $redis = Redis.new(:host => 'localhost', :port => 6379)
    end
  end
end

