ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
  :tls => true,
  :enable_starttls_auto => false,
  :port => 465,
  :address => "smtp.gmail.com",
  :domain => 'mindyourpitch.com',
  :user_name => "service@mindyourpitch.com",
  :password => "mindyourp!tch",
  :authentication => :plain,
  :openssl_verify_mode => 'none'
}
