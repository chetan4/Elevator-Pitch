# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Emanuel', :city => cities.first)

User.reset_column_information
Pitch.reset_column_information

users = User.create([
  {:name => "Vim Inc.", :password => "p@ssw0rd", :password_confirmation => "p@ssw0rd", :web_address => "http://vimcompanies.com/", :email => "jparekh@idyllic-software.com"},
  {:name => "Idyllic Software", :password => "p@ssw0rd", :password_confirmation => "p@ssw0rd", :web_address => "http://www.idyllic-software.com", :email => "info@idyllic-software.com"}
])

 vim_pitch = Pitch.new(
   :description => "We are solving the worlds biggest small business problem.\n\nImagine waking up in the morning and not knowing where to get your news from or get groceries or get your morning coffee from. It's easy for you as an individual because you exactly know which brands to buy from. Brands helps you buy with confidence. Small businesses do not have a brand they can goto for taking care of their daily chores.\n\nVim is launching hundreds of brands to make available one stop shop for small businesses for their daily chores such as accounting, software development and mentorship.\n\nWe believe small business impacts the economy in much larger way and has the capability to drive the economy in the right direction. Hence we want to make it easy for them to get there faster and become more successful.\n\nTell me, how can we help your small business today?",
   :user => users[0],
   :tags => 'Entrepreneur, startup, small business',
   :is_active => true
 )
 vim_pitch.sample = true
 vim_pitch.save!
 
 idyllic_pitch = Pitch.new(:description => "We make dreams happen for Tech Startups.\nWe believe building a business should be fun. If you could offshore your work to reap the offshoring benefits fearlessly, there could be nothing better. You could do more with less and increase your odds of success.\n\nIdyllic Software provides offshore web development services. To make it fearless and fun, we invite you here to India and work with us. Visit and enjoy the incredible India while we build your product in front of you. Yup, your trip is on us.\n\nEntrepreneurs have big dreams and little know-how or budget to make it a reality. Idyllic walks in as a partner and delivers the product on time, on budget and as envisioned.\n\nWe love startups and the passion of the entrepreneur. We are also entrepreneurs and we desire for our clients to have a successful business not to just write their code. Thats why we exclusively serve startups.", 
 :user => users[1], 
 :tags => 'Ruby on Rails, Web development, Ruby development', 
 :is_active => true)
 idyllic_pitch.sample = true
 idyllic_pitch.save!
  