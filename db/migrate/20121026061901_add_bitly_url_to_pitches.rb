class AddBitlyUrlToPitches < ActiveRecord::Migration
  def up
    add_column :pitches, :bitly_url, :string
  end

  def down
    remove_column :pitches, :bitly_url
  end
end
