class AddIdentifierToUsers < ActiveRecord::Migration
  def up
    add_column :users, :identifier, :string
    User.all.each{|c| c.update_attribute(:identifier, Devise.friendly_token[0,20]) } 
  end

  def down
    remove_column :users, :identifier
  end
end
