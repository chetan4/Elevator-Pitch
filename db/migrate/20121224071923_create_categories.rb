class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string  :title
      t.text :description
      t.timestamps
    end

    business_category = Category.create(:title => 'Business', :description => 'Your intend to reach out to your potential customers and make them aware of your product or services.')
    Category.create(:title => 'Investor', :description => 'You intend to reach out to potential investors to fund your business or idea.')
    Category.create(:title => 'Jobseeker', :description => 'You intend to explore available job opportunities in the market and be found by potential employers.')

    add_column :pitches, :category_id, :integer, :default => business_category.id
  end
end
