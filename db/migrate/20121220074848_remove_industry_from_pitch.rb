class RemoveIndustryFromPitch < ActiveRecord::Migration
  def up
    remove_column :pitches, :industry_id
  end

  def down
  end
end
