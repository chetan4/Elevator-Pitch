class AddSampleToPitches < ActiveRecord::Migration
  def change
    add_column :pitches, :sample, :boolean, :default => false
  end
end