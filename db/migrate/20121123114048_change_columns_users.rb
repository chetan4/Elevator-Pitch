class ChangeColumnsUsers < ActiveRecord::Migration
  def up
    remove_column(:users, :first_name)
    remove_column(:users, :last_name)
    remove_column(:users, :is_individual)
    remove_column(:users, :total_likes)
    add_column(:users, :name, :string)
    add_column(:users, :web_address, :string, :limit => 2303)
  end

  def down
    add_column(:users, :first_name, :string)
    add_column(:users, :last_name, :string)
    add_column(:users, :is_individual, :boolean)
    add_column(:users, :total_likes, :integer)
    remove_column(:users, :name)
    remove_column(:users, :web_address)
  end
end
