class DropPaymentsTable < ActiveRecord::Migration
  def up
    drop_table(:payments) if ActiveRecord::Base.connection.table_exists?(:payments)
  end

  def down
    #DO NOTHING
  end
end
