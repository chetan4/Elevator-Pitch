class AddTagsToPitch < ActiveRecord::Migration
  def change
    add_column :pitches, :tags, :text
  end
end
