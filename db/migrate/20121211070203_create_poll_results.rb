class CreatePollResults < ActiveRecord::Migration
  def change
    create_table :poll_results do |t|
      t.references :poll_question
      t.references :poll_answer
      t.references :user

      t.timestamps
    end
  end
end
