class AddIdentifierToPitches < ActiveRecord::Migration
  def up
    add_column :pitches, :identifier, :string
    Pitch.unscoped.all.each{|c| c.update_attribute(:identifier, Devise.friendly_token[0,20]) }
  end

  def down
    remove_column :pitches, :identifier
  end
end
