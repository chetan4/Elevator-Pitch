class AddIsActiveToPitches < ActiveRecord::Migration
  def up
    add_column :pitches, :is_active, :boolean, :default => false
  end

  def down
    remove_column :pitches, :is_active
  end
end
