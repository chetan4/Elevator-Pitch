class CreatePitchVersions < ActiveRecord::Migration
  def change
    create_table :pitch_versions do |t|
      t.references :pitch
      t.text :description

      t.timestamps
    end

    add_index(:pitch_versions, :pitch_id)
  end
end
