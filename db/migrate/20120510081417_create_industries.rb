class CreateIndustries < ActiveRecord::Migration
  def change
    create_table :industries do |t|
      t.string :name
      t.timestamps
    end
    
    if defined?(Industry)
      ["Energy","Industrials","Consumer Discretionary","Materials","Consumer staples","Financials","Telecommunication services","Health Care", "Information Technology","Utilities"].each do |name|
        Industry.create!(:name => name)
      end
    end
  end
end
