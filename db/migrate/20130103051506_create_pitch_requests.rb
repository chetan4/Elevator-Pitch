class CreatePitchRequests < ActiveRecord::Migration
  def change
    create_table :pitch_requests do |t|
      t.text :details
      t.references :user
      t.string :email
      t.references :category
      t.string :url
      t.timestamps
    end
  end
end
