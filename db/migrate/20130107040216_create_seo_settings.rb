class CreateSeoSettings < ActiveRecord::Migration
  def change
    create_table :seo_settings do |t|
      t.string :first_phrase
      t.string :first_url
      t.string :second_phrase
      t.string :second_url
      t.string :page_title
      t.string :page_description
      t.references :pitch
      t.timestamps
    end
  end
end
