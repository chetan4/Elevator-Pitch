class CreatePitches < ActiveRecord::Migration
  def change
    create_table :pitches do |t|
      t.text :description
      t.references :user
      t.references :industry
      t.timestamps
    end
  end
end
