class AddEmbedCodeToPitches < ActiveRecord::Migration
  def change
    add_column :pitches, :embed_code, :text
    add_column :pitches, :embed_url, :text
    add_column :pitches, :computer_width, :integer
    add_column :pitches, :computer_height, :integer
    add_column :pitches, :phone_width, :integer
    add_column :pitches, :phone_height, :integer
  end
end
