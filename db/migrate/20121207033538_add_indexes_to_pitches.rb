class AddIndexesToPitches < ActiveRecord::Migration
  def change
    add_index(:pitches, :user_id)
  end
end
