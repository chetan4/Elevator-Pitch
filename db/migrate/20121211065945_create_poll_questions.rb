class CreatePollQuestions < ActiveRecord::Migration
  def change
    create_table :poll_questions do |t|
      t.text :description
      t.boolean :active

      t.timestamps
    end
  end
end
