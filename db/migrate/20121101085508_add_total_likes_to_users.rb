class AddTotalLikesToUsers < ActiveRecord::Migration
  def up
    add_column :users, :total_likes, :integer
    User.all.each{ |u| u.update_attribute(:total_likes, u.likes.count) }
  end

  def down
    remove_column :users, :total_likes
  end
end
