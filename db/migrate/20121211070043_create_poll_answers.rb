class CreatePollAnswers < ActiveRecord::Migration
  def change
    create_table :poll_answers do |t|
      t.references :poll_question
      t.text :description

      t.timestamps
    end
  end
end
