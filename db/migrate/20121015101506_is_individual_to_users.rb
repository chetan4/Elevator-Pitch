class IsIndividualToUsers < ActiveRecord::Migration
  def up
    add_column :users, :is_individual, :boolean
  end

  def down
    remove_column :users, :is_individual
  end
end
